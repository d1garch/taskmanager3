package ru.pashintsev.tm;

import ru.pashintsev.tm.entity.Project;
import ru.pashintsev.tm.entity.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class App
{
    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_REMOVE = "project-remove";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_TASKS_LIST = "project-tasks-list";
    public static final String TASK_CREATE = "task-create";
    public static final String TASK_REMOVE = "task-remove";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String HELP = "help";
    public static final String EXIT = "exit";


    public static void main( String[] args ) {
        ProjectList projectList = new ProjectList();
        TaskList taskList = new TaskList();
        System.out.println("**** WELCOME TO TASK MANAGER ****");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            try {
                switch (reader.readLine()) {
                    case (PROJECT_CREATE):
                        projectCreate(projectList, reader);
                        break;
                    case (PROJECT_REMOVE):
                        projectRemove(projectList, taskList, reader);
                       break;
                    case (PROJECT_CLEAR) :
                        projectClear(projectList);
                        break;
                    case (PROJECT_LIST):
                        projectList(projectList);
                        break;
                    case (PROJECT_TASKS_LIST):
                        projectTasksList(reader, taskList);
                        break;
                    case (TASK_CREATE):
                        taskCreate(taskList, reader);
                        break;
                    case (TASK_REMOVE) :
                        taskRemove(taskList, reader);
                        break;
                    case (TASK_CLEAR):
                        taskClear(taskList);
                        break;
                    case (TASK_LIST):
                        taskList(taskList);
                        break;
                    case (HELP):
                        helpList();
                        break;
                    case (EXIT):
                        System.exit(0);
                    default:
                        System.out.println("[UNAVAILABLE COMMAND. ENTER 'help' TO SEE COMMAND LIST]");
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void projectCreate(ProjectList projectList, BufferedReader reader) throws IOException {
        System.out.println("[PROJECT CREATE]\nENTER NAME, DESCRIPTION, BEGIN DATE(DD.MM.YYYY), END DATE(DD.MM.YYYY):");
        projectList.addProject(new Project(reader.readLine(), reader.readLine(), reader.readLine(), reader.readLine()));
        System.out.println("[OK]");
    }

    public static void projectRemove(ProjectList projectList, TaskList taskList, BufferedReader reader) throws IOException {
        System.out.println("[PROJECT-REMOVE]\nENTER PROJECT NUMBER:");
        int indexPrj = Integer.parseInt(reader.readLine());
        projectList.removeProjectById(indexPrj-1);
        List<Task> arr = new ArrayList<>();
        for(Task task : taskList.showAllTasks()) {
            if(task.getProjectId() == indexPrj) {
                arr.add(task);
            }
        }
        for(int i = 0; i < arr.size(); i++) {
            taskList.removeTask(arr.get(i));
        }
        System.out.println("[OK]");
    }

    public static void projectClear(ProjectList projectList) {
        projectList.deleteAllProjects();
        System.out.println("[ALL PROJECTS HAS BEEN DELETED]");
    }

    public static void projectList(ProjectList projectList) {
        System.out.println("[PROJECT LIST]");
        if(!projectList.showAllProjects().isEmpty()) {
            for (int i = 0; i < projectList.showAllProjects().size(); i++) {
                System.out.println((i+1) + ". " + projectList.showAllProjects().get(i).getName());
            }
        }
        else System.out.println("[PROJECT LIST IS EMPTY]");
    }

    public static void projectTasksList(BufferedReader reader, TaskList taskList) throws IOException {
        System.out.println("[ENTER PROJECT NUMBER:]");
        Integer projectId = Integer.parseInt(reader.readLine());
        System.out.println("[TASKS IN PROJECT" + projectId + "]");
        for(Task task : taskList.showAllTasks()) {
            if(task.getProjectId() == projectId) {
                System.out.println(task.getName());
            }
        }
    }

    public static void taskCreate(TaskList taskList, BufferedReader reader) throws IOException {
        System.out.println("[TASK CREATE]\nENTER NAME, DESCRIPTION , BEGIN DATE(DD.MM.YYYY), END DATE(DD.MM.YYYY), PROJECT ACCESSORY NUMBER:");
        taskList.addTask(new Task(reader.readLine(), reader.readLine(), reader.readLine(), reader.readLine(), Integer.parseInt(reader.readLine())));
        System.out.println("[OK]");
    }

    public static void taskRemove(TaskList taskList, BufferedReader reader) throws IOException {
        System.out.println("[TASK REMOVE]\nENTER TASK NUMBER:");
        String lineTask = reader.readLine();
        int indexTask = Integer.parseInt(lineTask);
        taskList.removeTaskById(indexTask-1);
        System.out.println("[OK]");
    }

    public static void taskClear(TaskList taskList) {
        taskList.deleteAllTasks();
        System.out.println("[ALL TASKS HAS BEEN DELETED]");
    }

    public static void taskList(TaskList taskList) {
        System.out.println("[TASK LIST]");
        if(!taskList.showAllTasks().isEmpty()) {
            for (int i = 0; i < taskList.showAllTasks().size(); i++) {
                System.out.println((i+1) + ". " + taskList.showAllTasks().get(i).getName());
            }
        }
        else System.out.println("[TASK LIST IS EMPTY]");
    }

    public static void helpList() {
        System.out.println("'project-create' - adds new project to project list\n"+
                "'project-list' - shows all projects in project list\n"+
                "'project-tasks-list' - shows all tasks in project\n"+
                "'project-remove' - removes project from project list by number\n"+
                "'project-clear' - clears project list\n"+
                "'task-create' - adds new task to task list\n"+
                "'task-list' - shows all tasks in task list\n"+
                "'task-remove' - removes task from task list by number\n"+
                "'task-clear' - clears task list\n"+
                "'exit' - stops application\n"+
                "'help' - shows all available commands");
    }
}
