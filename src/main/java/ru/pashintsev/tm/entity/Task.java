package ru.pashintsev.tm.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Task {
    private long id;
    private String name;
    private String description;
    private String start;
    private String end;
    private Integer projectId;


    public Task(String name, String description, String start, String end, Integer projectId) {
        this.id++;
        this.name = name;
        this.description = description;
        this.start = start;
        this.end = end;
        this.projectId = projectId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
}
