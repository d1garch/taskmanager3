package ru.pashintsev.tm;

import ru.pashintsev.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectList {

    private final List<Project> projectList;


    public ProjectList() {

        this.projectList = new ArrayList<>();
    }

    public void addProject(Project project) {

        projectList.add(project);
    }

    public void removeProjectById(int id) {

        projectList.remove(id);
    }

    public List<Project> showAllProjects() {

        return projectList;
    }

    public void deleteAllProjects() {
        projectList.clear();
    }
}
