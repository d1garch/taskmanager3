package ru.pashintsev.tm;

import ru.pashintsev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskList {
    private final List<Task> tasks;

    public TaskList () {
        this.tasks = new ArrayList<>();
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public void removeTaskById(int id) {
        tasks.remove(id);
    }

    public List<Task> showAllTasks() {
        return tasks;
    }

    public void deleteAllTasks() {
        tasks.clear();
    }

    public void removeTask(Task task) {
        tasks.remove(task);
    }

}
